﻿using FPS.BusinessLayer.ViewModel.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public interface ICompanyRepository
    {
        public void AddCompany(CompanyModel model);

        void UpdateCompany(Guid id, CompanyModel model);

        CompanyModel GetCompanyById(Guid id); // New method

        IEnumerable<CompanyModel> GetAll();
        void DeleteCompany(Guid id);
    }
}
