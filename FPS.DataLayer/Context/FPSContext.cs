﻿using FPS.DataLayer.Config;
using FPS.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.DataLayer.Context
{
    public class FPSContext : DbContext
    {
        public FPSContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
        }

     
        public DbSet<Company> Companies { get; set; }
        public DbSet<Role> Roles { get; set; }
    }
}
